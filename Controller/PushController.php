<?php

namespace Arkulpa\AuthBundle\Controller;

use Arkulpa\AuthBundle\Entity\PushToken;
use Arkulpa\AuthBundle\Entity\User;
use Arkulpa\UtilsBundle\Controller\ExtendedController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class PushController extends ExtendedController
{
    /**
     * @Route("/savePushToken" ,name="arkulpa_user_save_push_token")
     * @Method("POST")
     */
    public function savePushTokenAction(Request $request)
    {
        try {
            /** @var User $user */
            $user = $this->getUser();
            if (!$user) {
                throw new \Exception('not-logged-in');
            }

            $pushToken = $request->get('pushToken');
            if (!$pushToken) {
                throw new \Exception('pushToken-is-missing');
            }

            $em = $this->getDoctrine()->getManager();
            $pushTokenRepo = $em->getRepository('ArkulpaAuthBundle:PushToken');

            $entity = $pushTokenRepo->findOneBy(array('token' => $pushToken));
            if ($entity) {
                //TODO is there a user check required?
                $em->remove($entity);
            }

            $pt = new PushToken();
            $pt->setToken($pushToken);
            $pt->setUser($user);

            $em->persist($pt);
            $em->flush();

            return new JsonResponse(true);
        } catch (\Exception $e) {
            return $this->generateLogicErrorResponse($e);
        }

    }


}
