<?php

namespace Arkulpa\AuthBundle\Controller;

use Arkulpa\AuthBundle\Entity\User;
use Arkulpa\AuthBundle\Form\UserAuthenticateValidateType;
use Arkulpa\UtilsBundle\Controller\ExtendedController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class LoginController extends ExtendedController
{
    /**
     * @Route("/login")
     * @Method("POST")
     */
    public function loginUserAction(Request $request)
    {

        try {
            $data = $request->request->all();
            $formFactory = $this->get('form.factory');
            $form = $formFactory->create(
                'Arkulpa\AuthBundle\Form\UserAuthenticateValidateType',
                null,
                array('csrf_protection' => false,)
            );

            $validationErrors = $this->get('arkulpa.form_validator')->customBindAndValidate($form, $request);
            if ($validationErrors !== null) {
                return $this->generateValidationErrorResponse($validationErrors);
            }

            /** @var User $user */
            $user = $this->get('arkulpa.user_repository')->getUserByUsername(
                $data['username'],
                true
            );
            if (!$user) {
                return $this->generateCredentialsErrorResponse('arkulpa.user.not.found');
            }

            $date = new \DateTime();
            if ($user->getLicenseEndTs() !== null && $user->getLicenseEndTs()->getTimestamp() <= $date->getTimestamp()) {
                return new JsonResponse(array('error' => array('message' => 'licenceExpired')), 423);
            }

            $encoder = $this->get('security.encoder_factory')->getEncoder($user);
            $hashedPassword = $encoder->encodePassword($data['password'], $user->getSalt());
            if ($user->getPassword() !== $hashedPassword) {
                return $this->generateCredentialsErrorResponse('arkulpa.user.password.invalid');
            }

            if ($user->getAuthToken() == null) {
                $token = $this->get('arkulpa.token_generator')->generate();
                $user->setAuthToken($token);
                $em = $this->getDoctrine()->getManager();
                $em->flush();
            }

            return $this->generateSuccesResponse(
                array(
                    'id' => $user->getId(),
                    'authToken' => $user->getAuthToken(),
                    'username' => $user->getUserName(),
                    'email' => $user->getEmail(),
                    'firstname' => $user->getFirstName(),
                    'lastname' => $user->getLastName(),
                    'phone' => $user->getPhone(),
                    'role' => $user->getRoles()[0],
                    'type' => $user->getType(),
                    'locale' => $user->getLocale()
                )
            );

        } catch (\Exception $e) {
            return $this->generateLogicErrorResponse($e);
        }
    }

}
