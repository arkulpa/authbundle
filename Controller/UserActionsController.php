<?php

namespace Arkulpa\AuthBundle\Controller;

use Arkulpa\AuthBundle\Entity\PushToken;
use Arkulpa\AuthBundle\Entity\User;
use Arkulpa\UtilsBundle\Controller\ExtendedController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class UserActionsController extends ExtendedController
{
    /**
     * @Route("/activate/{token}",name="arkulpa_user_activate")
     * @Method("GET")
     * @param $token
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function activateUserAction($token)
    {
        $error = null;
        try {
            $em = $this->getDoctrine()->getManager();
            $rep = $em->getRepository('ArkulpaAuthBundle:User');
            $user = $rep->getUserByChangeToken($token);
            if (!$user) {
                throw new \Exception('invalid-change-token');
            }
            $currentTs = new \DateTime();

            if ($user->getChangeTokenValidUntil() == null ||
                $user->getChangeTokenValidUntil()->getTimestamp() < $currentTs->getTimestamp()
            ) {
                throw new \Exception('expired-change-token');
            }

            $user->setActive(true);
            $user->setChangeToken(null);
            $user->setChangeTokenValidUntil(null);
            $em->flush();
            $em->clear();

        } catch (\Exception $e) {
            $error = $this->get('translator')->trans($e->getMessage());
        }

        return $this->render('ArkulpaAPIBundle:Templates:userRegisterActivation.html.twig', array('error' => $error));
    }


    /**
     * @Route("/register")
     * @Method("POST")
     */
    public function registerUserAction(Request $request)
    {
        try {
            $data = $request->get('api_register');
            if (isset($data['defaultActive'])) {
                $defaultActive = true;
                unset($data['defaultActive']);
            } else {
                $defaultActive = false;
            }

            $request->request->set('api_register', $data);

            $form = $this->get('form.factory')->create(
                new UserRegisterValidateType(),
                null,
                array('csrf_protection' => false,)
            );


            $validationErrors = $this->get('arkulpa_form_validator')->bindAndValidate($form, $request);
            if ($validationErrors !== null) {
                return $this->generateValidationErrorResponse($validationErrors);
            }

            $data = $request->get('api_register');
            $userName = $data['email']['first'];
            $barberHash = $data['barberHash'];

            $barber = $this->getDoctrine()->getRepository('ArkulpaAPIBundle:Barber')->findOneBy(
                array('hash' => $barberHash)
            );
            if (!$barber) {
                throw new \Exception('barber-does-not-exit');
            }

            $user = $this->get('barber.user_repository')->getUserByUsername($barberHash, $userName);
            if ($user) {
                $error = new \stdClass();
                $error->id = 'email';
                $error->message = 'E-Mail Adresse ist bereits Registiert, bitte melden Sie Sich an.';
                return $this->generateValidationErrorResponse(array($error));
            }

            //Insert new User
            $u = new User();
            $u->setActive(false);

            $u->setUsername($userName);

            $u->setSalt(md5(time()));
            $encoder = $this->get('security.encoder_factory')->getEncoder($u);
            $u->setPassword($encoder->encodePassword($data['password']['first'], $u->getSalt()));


            $u->setEmail($data['email']['first']);
            $u->setFirstName($data['firstName']);
            $u->setLastName($data['lastName']);
            $u->setPhone($data['phone']);

            $u->setRegistered(new \DateTime());

            $tokenGenerator = $this->get('token_generator');
            $u->setChangeToken($tokenGenerator->generate());
            $validChangeDate = new \DateTime();
            $u->setChangeTokenValidUntil($validChangeDate->add(new \DateInterval('P3D')));

            $u->setRoles(array('ROLE_USER'));
            $u->setBarber($barber);

            $em = $this->getDoctrine()->getManager();
            if ($defaultActive == true) {
                $u->setActive(true);
                $u->setAuthToken($tokenGenerator->generate());
            }

            $em->persist($u);
            $em->flush();
            $em->clear();

            $this->get('barber.email.notify.service')->sendRegisterMail($request, $u);
            if ($defaultActive == false) {
                return $this->generateSuccesResponse();
            }
            return $this->generateSuccesResponse($u->getAuthToken());
        } catch (\Exception $e) {
            return $this->generateLogicErrorResponse($e);
        }
    }

    /**
     * @Route("/logout")
     * @Method("POST")
     */
    public function logoutUserAction(Request $request)
    {
        try {
            if ($request->request->has('authToken')) {
                $em = $this->getDoctrine()->getManager();
                $authToken = $request->request->get('authToken');

                $user = $em->getRepository('ArkulpaAuthBundle:User')->getUserByAuthToken($authToken);
                if ($user) {
                    //for security remove the authToken, on new login a new is created!
                    $user->setAuthToken(null);
                    $em->flush();
                }
            }
            return $this->generateSuccesResponse();
        } catch (\Exception $e) {
            return $this->generateLogicErrorResponse($e);
        }

    }


}
