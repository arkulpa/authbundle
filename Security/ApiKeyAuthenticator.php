<?php

namespace Arkulpa\AuthBundle\Security;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\SimplePreAuthenticatorInterface;


class ApiKeyAuthenticator implements SimplePreAuthenticatorInterface, AuthenticationFailureHandlerInterface
{
    protected $userProvider;


    public function __construct(UserProvider $userProvider)
    {
        $this->userProvider = $userProvider;
    }

    public function createToken(Request $request, $providerKey)
    {
        if ($request->headers->has('Authorization')) {
            $token = $request->headers->get('Authorization');
        } else {
            $token = $request->get('authToken');
        }

        if (!$token) {
            throw new BadCredentialsException('No API key found');
        }

        return new PreAuthenticatedToken(
            'anon.',
            array(
                'authToken' => $token
            ),
            $providerKey
        );
    }

    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        $data = $token->getCredentials();
        $user = $this->userProvider->getUserForApiKey($data['authToken']);
        if (!$user) {
            throw new AuthenticationException(
                sprintf('API Key "%s" does not exist.', $data['authToken'])
            );
        }

        $date = new \DateTime();
        if ($user->getLicenseEndTs() !== null && $user->getLicenseEndTs()->getTimestamp() <= $date->getTimestamp()) {
            throw new AuthenticationException('licenceExpired');
        }

        return new PreAuthenticatedToken(
            $user,
            $data,
            $providerKey,
            $user->getRoles()
        );
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof PreAuthenticatedToken && $token->getProviderKey() === $providerKey;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {

        if ($exception->getMessage() == 'licenceExpired') {
            return new JsonResponse(array('error' => array('message' => 'licenceExpired')), 423);
        }

        $data = array('error' => array('message' => 'Login invalid'));
        return new JsonResponse($data, 401);
    }


}
