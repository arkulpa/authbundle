<?php
/**
 * User: TVO
 * Date: 06.05.14
 * Time: 11:47
 */

namespace Arkulpa\AuthBundle\Security;

use Arkulpa\AuthBundle\Repository\UserRepository;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider implements UserProviderInterface
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param $apiKey
     * @return User|null
     */
    public function getUserForApiKey($apiKey)
    {
        return $this->userRepository->getUserByAuthToken($apiKey);
    }

    /**
     * @param $username
     * @return null|User
     */
    public function loadUserByUsername($username)
    {
        return $this->userRepository->findOneBy(array('username' => $username));
    }

    public function refreshUser(UserInterface $user)
    {
        return $this->userRepository->find($user->getId());
    }

    public function supportsClass($class)
    {
        return 'Arkulpa\AuthBundle\Entity\User' === $class;
    }
}
