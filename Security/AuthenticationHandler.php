<?php
/**
 * Handler for AJAX Authentication form
 */

namespace Arkulpa\APIBundle\Security;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Translation\Translator;

class AuthenticationHandler implements AuthenticationSuccessHandlerInterface, AuthenticationFailureHandlerInterface
{

    private $router;
    private $session;
    private $translator;
    private $doctrine;
    private $securityContext;

    public function __construct(
        RouterInterface $router,
        Session $session,
        Translator $translator,
        Registry $doctrine,
        SecurityContextInterface $securityContext
    )
    {
        $this->router = $router;
        $this->session = $session;
        $this->translator = $translator;
        $this->doctrine = $doctrine;
        $this->securityContext = $securityContext;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        // if AJAX login
        if ($request->isXmlHttpRequest()) {
            $array = array('success' => true); // data to return via JSON
            $response = new Response(json_encode($array));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
            // if form login
        } else {
            if ($this->session->get('_security.main.target_path')) {
                $url = $this->session->get('_security.main.target_path');
            } else {
                $url = $this->router->generate('hoval_top_tronic_homepage');
            } // end if
            return new RedirectResponse($url);
        }
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        // if AJAX login
        if ($request->isXmlHttpRequest()) {
            $array = array(
                'success' => false,
                'message' => $this->translator->trans('user-login-failed')
            ); // data to return via JSON
            $response = new Response(json_encode($array), 400);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
            // if form login
        } else {
            // set authentication exception to session
            $request->getSession()->set(SecurityContextInterface::AUTHENTICATION_ERROR, $exception);
            return new RedirectResponse($this->router->generate('login'));
        }
    }


}
