<?php
namespace Arkulpa\AuthBundle\Service;

use Arkulpa\AuthBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Exception\ValidationFailedException;
use Symfony\Bridge\Doctrine\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Exception\ValidatorException;

class UserService
{

    /** @var  ManagerRegistry */
    private $em;
    /** @var  ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->em = $container->get('doctrine')->getManager();
    }

    public function saveUser($request, $options = [])
    {
        $id = $request->get('id');
        if ($id == null) {
            $user = new User();
            $user->setRegistered(new \DateTime());
        } else {
            $user = $this->em->getRepository('ArkulpaAuthBundle:User')->find($id);
        }
        $user->setActive($request->get('active', false));
        $user->setType($request->get('type'));

        $user->setUsername($request->get('username'));
        if ($request->get('username', '') === '' && isset($options['generateUsername'])) {
            $latestId = $this->em->getRepository('ArkulpaAuthBundle:User')->getLatestUserId();
            $user->setUsername($options['generateUsername'] . ($latestId + 1));
        }
        $user->setFirstName($request->get('firstName'));
        $user->setLastName($request->get('lastName'));
        $user->setPhone($request->get('phone'));

        $user->setEmail($request->get('email'));
        //if user has no Email --> this Fallback
        if (($user->getEmail() === "" || $user->getEmail() === null) && isset($options['generateEmail'])) {
            $user->setEmail($user->getUsername() . $options['generateEmail']);
        }

        if ($request->get('plainPassword') !== null && $request->get('plainPassword') !== '') {
            $user->setPlainPassword($request->get('plainPassword'));
            $user->setSalt(md5(time()));
            $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
            $user->setPassword($encoder->encodePassword($request->get('plainPassword'), $user->getSalt()));
        }

        $errors = $this->container->get('validator')->validate($user);
        if ($errors->count() > 0) {
            throw new ValidationFailedException($errors);
        }

        $user->setRoles(['ROLE_USER']);
        $user->setActive(true);
        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }


    public function deleteUser(User $user, EntityManager $em = null)
    {
        $flush = false;
        if ($em == null) {
            $em = $this->$em;
            $flush = true;
        }
        $user->setUsername(time() . '_' .$user->getUsername());
        $user->setEmail(time() . '_' .$user->getEmail());
        $user->setActive(false);
        $user->setDeletedTs(new \DateTime());

        if ($flush) {
            $em->flush();
        }
    }
}
