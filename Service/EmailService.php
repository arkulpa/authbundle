<?php
namespace Arkulpa\AuthBundle\Service;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\Translator;

class EmailService
{

    /** @var \Swift_Mailer */
    private $mailer;
    /** @var  \Twig_Environment */
    private $twig;
    /** @var  Translator */
    private $translator;


    //TODO: get from config!
    private $notifyAddresses = 'thomas@arkulpa.at';
    private $senderAddress = 'versand@arkulpa.at';

    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $twig, Translator $translator)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->translator = $translator;
    }

    /**
     * @param null $sender
     * @return \Swift_Message
     */
    private function generateEmptyHtmlMessage($sender = null)
    {
        $message = \Swift_Message::newInstance();
        if ($sender === null) {
            $message->setFrom($this->senderAddress);
        } else {
            $message->setFrom($sender);
        }
        $message->setContentType('text/html');
        $message->setCharset('utf-8');
        return $message;
    }

    public function sendMail($subject, $msg, $receivers, $sender = null)
    {
        $message = $this->generateEmptyHtmlMessage($sender);
        $message->setSubject($subject);
        foreach (explode(';', $receivers) as $a) {
            $message->setTo($a);
        }
        $message->setBody($msg, 'text/html');
        $this->mailer->send($message);
    }

    public function sendMailExtended($subject, $msg, $msgPlain, $receivers, $sender = null, $replyTo = null)
    {
        $message = $this->generateEmptyHtmlMessage($sender);
        if ($replyTo !== null) {
            $message->setReplyTo($replyTo);
        }
        $message->setSubject($subject);
        foreach (explode(';', $receivers) as $a) {
            $message->setTo($a);
        }
        $message->setBody($msgPlain);
        $message->addPart($msg, 'text/html');
        $this->mailer->send($message);
    }

    public function sendRegisterMail(Request $request, User $user)
    {
        $message = $this->generateEmptyHtmlMessage();
        $message->setTo($user->getEmail());
        $message->setSubject($this->translator->trans('register-subject'));

        $host = $request->headers->get('host');
        $message->setBody(
            $this->twig->render(
                'ArkulpaAPIBundle:Mail:register.html.twig',
                array('user' => $user, 'host' => $host)
            )
        );
        $this->mailer->send($message);
    }

    public function sendLostPasswordMail(Request $request, User $user)
    {
        $message = $this->generateEmptyHtmlMessage();
        $message->setTo($user->getEmail());
        $message->setSubject($this->translator->trans('lost-password-subject'));

        $host = $request->headers->get('host');
        $message->setBody(
            $this->twig->render(
                'ArkulpaAPIBundle:Mail:lost-password.html.twig',
                array('user' => $user, 'host' => $host)
            )
        );

        $this->mailer->send($message);
    }

}
