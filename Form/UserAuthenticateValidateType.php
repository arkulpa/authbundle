<?php
/**
 * Validate input for Api Authenicate
 */

namespace Arkulpa\AuthBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserAuthenticateValidateType extends AbstractType
{

    public function __construct()
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'username',
            null,
            array(
                'constraints' => array(
                    new NotBlank(array('message' => 'username-empty-error')),
                ),
            )
        );
        $builder->add(
            'password',
            null,
            array(
                'constraints' => array(
                    new NotBlank(array('message' => 'password-empty-error')),
                ),
            )
        );
    }

    public function getName()
    {
        return 'arkulpa_login_form';
    }
}
