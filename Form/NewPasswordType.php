<?php

namespace Arkulpa\AuthBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class NewPasswordType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'password1',
                null,
                array(
                    'label' => 'password1',
                    'attr' => array(),
                    'constraints' => array(
                        new NotBlank(array('message' => 'FEHLER: BITTE FÜLLE BEIDE PASSWORTFELDER AUS')),
                        new Length(array('min' => 6, 'minMessage' => 'password-min-length-error'))
                    )
                )
            )
            ->add(
                'password2',
                null,
                array(
                    'label' => 'password2',
                    'attr' => array(),
                    'constraints' => array(
                    )
                )
            )
            ->add(
                'privateKey',
                'hidden',
                array(
                    'label' => 'privateKey',
                    'attr' => array(),
                    'constraints' => array(
                        new NotBlank(array('message' => 'Token erforderlich')),
                    )
                )
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'allow_extra_fields' => true,
                'csrf_protection' => false
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'new_password_form';
    }
}
