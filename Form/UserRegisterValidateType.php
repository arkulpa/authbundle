<?php

namespace Arkulpa\AuthBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserRegisterValidateType extends AbstractType
{

    public function __construct()
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
            'firstName',
            null,
            array(
                'constraints' => array(
                    new NotBlank(array('message' => 'user-first-name-empty-error'))
                ),
            )
        );
        $builder->add(
            'lastName',
            null,
            array(
                'constraints' => array(
                    new NotBlank(array('message' => 'user-last-name-empty-error'))
                ),
            )
        );

        $builder->add(
            'email',
            null,
            array(
                'type' => 'email',
                'constraints' => array(
                    new NotBlank(array('message' => 'user-email-empty-error')),
                    new Email(array('message' => 'user-email-invalid-email-error'))
                ),
            )
        );

        $builder->add(
            'phone',
            null,
            array(
                'constraints' => array(
                    new NotBlank(array('message' => 'user-phone-empty-error'))
                ),
            )
        );
        $builder->add(
            'username',
            null,
            array(
                'constraints' => array(
                    new NotBlank(array('message' => 'username-empty-error'))
                ),
            )
        );
        $builder->add(
            'password',
            null,
            array(
                'constraints' => array(
                    new NotBlank(array('message' => 'user-password-empty-error')),
                ),
            )
        );

        $builder->add(
            'password',
            'repeated',
            array(
                'type' => 'password',
                'invalid_message' => 'user-password-not-matching',
                'first_options' => array(
                    'constraints' => array(
                        new NotBlank(array('message' => 'user-password-empty-error')),
                    )
                ),
                'second_options' => array(
                    'constraints' => array(
                        new NotBlank(array('message' => 'user-password2-empty-error')),
                    )
                ),
            )
        );

    }

    public function getName()
    {
        return 'arkulpa_register';
    }
}
