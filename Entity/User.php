<?php

namespace Arkulpa\AuthBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table("a_user",uniqueConstraints={@ORM\UniqueConstraint(name="name_idx", columns={"username", "email"})}))
 * @ORM\Entity(repositoryClass="Arkulpa\AuthBundle\Repository\UserRepository")
 * @UniqueEntity(fields={"username"}, message="arkulpa.user.username.not.unique")
 * @UniqueEntity(fields={"email"}, message="arkulpa.user.email.not.unique")
 */
class User implements UserInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="arkulpa.user.username.blank")
     * @Assert\Length(min = 4)
     * @ORM\Column(name="username", type="string", length=255, unique=true)
     */
    private $username;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Email()
     * @ORM\Column(name="email", type="string", length=255, nullable=true, unique=true)
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(name="firstName", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=15, nullable=true)
     */
    private $phone;

    /**
     * @var array
     *
     * @ORM\Column(name="roles", type="json_array", nullable=false)
     */
    private $roles;

    /**
     * @var string
     * @Exclude()
     * @Assert\NotBlank()
     * @ORM\Column(name="salt", type="string", length=255, nullable=true)
     */
    private $salt;

    /**
     * @var string
     * @Exclude()
     * @Assert\NotBlank()
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @var string
     * @ORM\Column(name="plainPassword", type="string", length=255, nullable=true)
     */
    private $plainPassword;

    /**
     * @var string
     * @Exclude()
     * @ORM\Column(name="authToken", type="string", length=255, nullable=true)
     */
    private $authToken;

    /**
     * @var \DateTime
     * @Exclude()
     * @ORM\Column(name="authTokenValidUntil", type="datetime",nullable=true)
     */
    private $authTokenValidUntil;

    /**
     * @var string
     * @Exclude()
     * @ORM\Column(name="changeToken", type="string", length=255, nullable=true)
     */
    private $changeToken;

    /**
     * @var \DateTime
     * @Exclude()
     * @ORM\Column(name="changeTokenValidUntil", type="datetime",nullable=true)
     */
    private $changeTokenValidUntil;


    /**
     * @var string
     * @Exclude()
     * @ORM\Column(name="resetToken", type="string", length=255, nullable=true)
     */
    private $resetToken;

    /**
     * @var \DateTime
     * @Exclude()
     * @ORM\Column(name="resetTokenValidUntil", type="datetime",nullable=true)
     */
    private $resetTokenValidUntil;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="registered", type="datetime")
     */
    private $registered;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    /**
     * @var \DateTime
     * @Exclude()
     * @ORM\Column(name="deletedTs", type="datetime",nullable=true)
     */
    private $deletedTs;


    /**
     * @var \Doctrine\Common\Collections\Collection|PushToken[]
     * @ORM\OneToMany(targetEntity="PushToken", mappedBy="user")
     */

    private $pushTokens;


    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=2, nullable=true)
     */
    private $type;


    /**
     * @var \DateTime
     * @Exclude()
     * @ORM\Column(name="LicenseEndTs", type="datetime",nullable=true)
     */
    private $licenseEndTs;

    /**
     * @var string
     * @Exclude()
     * @ORM\Column(name="locale", type="string", length=5, nullable=true)
     */
    private $locale;


    public function __construct()
    {
        $this->roles = array('user');
        $this->active = false;
    }

    public function __toString()
    {
        return $this->getUsername();
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set authToken
     *
     * @param string $authToken
     * @return User
     */
    public function setAuthToken($authToken)
    {
        $this->authToken = $authToken;

        return $this;
    }

    /**
     * Get authToken
     *
     * @return string
     */
    public function getAuthToken()
    {
        return $this->authToken;
    }

    /**
     * Set roles
     *
     * @param array $roles
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Get roles
     *
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }


    /**
     * Set active
     *
     * @param boolean $active
     * @return User
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set changeToken
     *
     * @param string $changeToken
     * @return User
     */
    public function setChangeToken($changeToken)
    {
        $this->changeToken = $changeToken;

        return $this;
    }

    /**
     * Get changeToken
     *
     * @return string
     */
    public function getChangeToken()
    {
        return $this->changeToken;
    }

    /**
     * Set changeTokenValidUntil
     *
     * @param \DateTime $changeTokenValidUntil
     * @return User
     */
    public function setChangeTokenValidUntil($changeTokenValidUntil)
    {
        $this->changeTokenValidUntil = $changeTokenValidUntil;

        return $this;
    }

    /**
     * Get changeTokenValidUntil
     *
     * @return \DateTime
     */
    public function getChangeTokenValidUntil()
    {
        return $this->changeTokenValidUntil;
    }

    /**
     * Set registered
     *
     * @param \DateTime $registered
     * @return User
     */
    public function setRegistered($registered)
    {
        $this->registered = $registered;

        return $this;
    }

    /**
     * Get registered
     *
     * @return \DateTime
     */
    public function getRegistered()
    {
        return $this->registered;
    }

//    public function isEqualTo(UserInterface $user)
//    {
//        if (!$user instanceof User) {
//            return false;
//        }
//
//        if ($this->password !== $user->getPassword()) {
//            return false;
//        }
//
//        if ($this->salt !== $user->getSalt()) {
//            return false;
//        }
//
//        if ($this->username !== $user->getUsername()) {
//            return false;
//        }
//
//        return true;
//    }


    /**
     * Set resetToken
     *
     * @param string $resetToken
     *
     * @return User
     */
    public function setResetToken($resetToken)
    {
        $this->resetToken = $resetToken;

        return $this;
    }

    /**
     * Get resetToken
     *
     * @return string
     */
    public function getResetToken()
    {
        return $this->resetToken;
    }

    /**
     * Set resetTokenValidUntil
     *
     * @param \DateTime $resetTokenValidUntil
     *
     * @return User
     */
    public function setResetTokenValidUntil($resetTokenValidUntil)
    {
        $this->resetTokenValidUntil = $resetTokenValidUntil;

        return $this;
    }

    /**
     * Get resetTokenValidUntil
     *
     * @return \DateTime
     */
    public function getResetTokenValidUntil()
    {
        return $this->resetTokenValidUntil;
    }

    /**
     * Set authTokenValidUntil
     *
     * @param \DateTime $authTokenValidUntil
     *
     * @return User
     */
    public function setAuthTokenValidUntil($authTokenValidUntil)
    {
        $this->authTokenValidUntil = $authTokenValidUntil;

        return $this;
    }

    /**
     * Get authTokenValidUntil
     *
     * @return \DateTime
     */
    public function getAuthTokenValidUntil()
    {
        return $this->authTokenValidUntil;
    }

    /**
     * Set plainPassword
     *
     * @param string $plainPassword
     *
     * @return User
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * Get plainPassword
     *
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * Add pushToken
     *
     * @param \Arkulpa\AuthBundle\Entity\PushToken $pushToken
     *
     * @return User
     */
    public function addPushToken(\Arkulpa\AuthBundle\Entity\PushToken $pushToken)
    {
        $this->pushTokens[] = $pushToken;

        return $this;
    }

    /**
     * Remove pushToken
     *
     * @param \Arkulpa\AuthBundle\Entity\PushToken $pushToken
     */
    public function removePushToken(\Arkulpa\AuthBundle\Entity\PushToken $pushToken)
    {
        $this->pushTokens->removeElement($pushToken);
    }

    /**
     * Get pushTokens
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPushTokens()
    {
        return $this->pushTokens;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return User
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set deletedTs
     *
     * @param \DateTime $deletedTs
     *
     * @return User
     */
    public function setDeletedTs($deletedTs)
    {
        $this->deletedTs = $deletedTs;

        return $this;
    }

    /**
     * Get deletedTs
     *
     * @return \DateTime
     */
    public function getDeletedTs()
    {
        return $this->deletedTs;
    }

    /**
     * Set licenseEndTs
     *
     * @param \DateTime $licenseEndTs
     *
     * @return User
     */
    public function setLicenseEndTs($licenseEndTs)
    {
        $this->licenseEndTs = $licenseEndTs;

        return $this;
    }

    /**
     * Get licenseEndTs
     *
     * @return \DateTime
     */
    public function getLicenseEndTs()
    {
        return $this->licenseEndTs;
    }

    /**
     * Set locale
     *
     * @param string $locale
     *
     * @return User
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }
}
