<?php

namespace Arkulpa\AuthBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PushToken
 *
 * @ORM\Table(name="a_push_token")
 * @ORM\Entity(repositoryClass="Arkulpa\AuthBundle\Repository\PushTokenRepository")
 */
class PushToken
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255, unique=true)
     */
    private $token;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=1)
     */
    private $type;


    /**
     * @ORM\ManyToOne(targetEntity="\Arkulpa\AuthBundle\Entity\User",inversedBy="pushTokens")
     * @ORM\JoinColumn(name="user",nullable=false)
     */

    private $user;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return PushToken
     */
    public function setToken($token)
    {

        $this->token = $token;

        if (strlen($token) > 100) {
            $this->setType('A');
        } else {
            $this->setType('I');
        }

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return PushToken
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set user
     *
     * @param \Arkulpa\AuthBundle\Entity\User $user
     *
     * @return PushToken
     */
    public function setUser(\Arkulpa\AuthBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Arkulpa\AuthBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
