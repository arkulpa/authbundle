<?php

namespace Arkulpa\AuthBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserRepository extends EntityRepository
{

    /**
     * @param $username
     * @param bool $active
     * @return User|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getUserByUsername($username, $active = false)
    {
        $qb = $this->getEntityManager()->createQueryBuilder()->select('u')
            ->from('ArkulpaAuthBundle:User', 'u')
            ->where('u.username = :username')->setParameter('username', $username);
        if ($active) {
            $qb->andWhere('u.active = :active')->setParameter('active', $active);
        }
        try {
            return $qb->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        }
    }


    public function getUserByIdWithPlainPassword($id)
    {
        $qb = $this->getEntityManager()->createQueryBuilder()->select('u.id,u.username, u.firstName, u.lastName,u.plainPassword, u.email,u.phone')
            ->from('ArkulpaAuthBundle:User', 'u')
            ->where('u.id = :id')->setParameter('id', $id);
        try {
            return $qb->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        }
    }


    public function getLatestUserId()
    {
        $qb = $this->getEntityManager()->createQueryBuilder()->select('u.id')
            ->from('ArkulpaAuthBundle:User', 'u')
            ->orderBy('u.id', 'DESC')
            ->setMaxResults(1);
        try {
            return $qb->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            return 0;
        }
    }


    /**
     * @param $authToken
     * @return User|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getUserByAuthToken($authToken)
    {


        $qb = $this->getEntityManager()->createQueryBuilder()->select('u')
            ->from('ArkulpaAuthBundle:User', 'u')
            ->where('u.authToken = :token')->setParameter('token', $authToken)
            ->andWhere('u.active = :active')->setParameter('active', true);
        try {
            return $qb->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
     * @param $token
     * @return null|User
     */
    public function getUserByChangeToken($token)
    {
        return $this->findOneBy(array('changeToken' => $token));
    }

    /**
     * @param $authToken
     * @return null|string
     */
    public function getUserNameByAuthToken($authToken)
    {
        $user = $this->getUserByAuthToken($authToken);
        if (!$user) {
            return null;
        }
        return $user->getUsername();
    }

    /**
     * Loads the user for the given username.
     *
     * This method must throw UsernameNotFoundException if the user is not
     * found.
     *
     * @param string $username The username
     *
     * @return UserInterface
     *
     * @see UsernameNotFoundException
     *
     * @throws UsernameNotFoundException if the user is not found
     */
    public function loadUserByUsername($username)
    {
        return $this->findOneBy(array('username' => $username));
    }

    /**
     * Refreshes the user for the account interface.
     *
     * It is up to the implementation to decide if the user data should be
     * totally reloaded (e.g. from the database), or if the UserInterface
     * object can just be merged into some internal array of users / identity
     * map.
     *
     * @param UserInterface $user
     *
     * @return UserInterface
     *
     * @throws UnsupportedUserException if the account is not supported
     */
    public function refreshUser(UserInterface $user)
    {
        return $this->find($user->getId());
    }

    /**
     * Whether this provider supports the given user class.
     *
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return 'Arkulpa\APIBundle\Entity\User' === $class;
    }

    public function getAllAdminUsers()
    {
        $qb = $this->getEntityManager()->createQueryBuilder()->select('u')
            ->from('ArkulpaAuthBundle:User', 'u')
            ->where('u.roles like :role')->setParameter('role', '%ADMIN%');
        return $qb->getQuery()->getResult();
    }
}
